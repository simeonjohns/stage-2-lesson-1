function rotateMatrix(matrix_input) {
	if (!(matrix_input) || !(matrix_input.length)) { // Check if input is an array
		console.log('Input is not an array');
		return;
	}

	var matrix_output = [],
		lenI = matrix_input.length,
		lenJ = matrix_input[0].length;
	
	if (!(lenJ)) {									// Check if input is a 2D array
		console.log('Input is not a 2D array');
		return;
	}

	for (var j = 0; j < lenJ; j++) {
		matrix_output.push([]);

		for (var i = 0; i < lenI; i++) {
			matrix_output[j].unshift(matrix_input[i][j]);
		}
	}

	return matrix_output;
}